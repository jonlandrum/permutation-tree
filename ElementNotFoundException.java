public class ElementNotFoundException extends RuntimeException {
	public ElementNotFoundException (String collection) {
		System.out.println ("The target element is not in this collection");
	}
}