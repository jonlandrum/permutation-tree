public class Driver {
    public static void main (String[] args) {
        // Create the permutation tree with the number ``123456789''
        PermutationTree tree = new PermutationTree(123456789);
        
        // Print the resulting tree
        System.out.println(tree);
    }
}